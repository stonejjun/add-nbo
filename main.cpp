#include <netinet/in.h>
#include <stdio.h>
#include <stdint.h>
using namespace std;
/*
typedef long long int ll;
typedef long double dl;
typedef pair<dl,dl> pdi;
typedef pair<ll,ll> pii;
typedef pair<ll,pii> piii;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
//cout<<fixed;
//cout.precision(12);
 template by stonejjun 
*/

int main(int argc, char* argv[]){

	FILE* fp = fopen(argv[1], "r");
	FILE* fp2 = fopen(argv[2], "r");
	uint32_t a,b,res;

    fread(&a, sizeof(a), 1, fp);
    fread(&b, sizeof(b), 1, fp2);

    fclose(fp);
    fclose(fp2);

    a=ntohl(a);
    b=ntohl(b);

    res=a+b;
    printf("%u(0x%x) + %u(0x%x) = %u(0x%x)\n",a,a,b,b,res,res);

    return 0;
}